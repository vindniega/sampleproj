package com.collabera.shoppingapp.controller;

import com.collabera.shoppingapp.model.Product;
import com.collabera.shoppingapp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/create")
    public ResponseEntity<Object> createPost(@RequestBody Product product){
        productService.createProduct(product);
        return new ResponseEntity<>("You created a product!", HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<Object> getItems(){
        return new ResponseEntity<>(productService.getProduct(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateItem(@PathVariable Long id, @RequestBody Product product){
        productService.updateProduct(id, product);
        return new ResponseEntity<>("Updated!", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteItem(@PathVariable Long id){
        productService.deleteProduct(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
