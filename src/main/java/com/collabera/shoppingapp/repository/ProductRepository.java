package com.collabera.shoppingapp.repository;

import com.collabera.shoppingapp.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
