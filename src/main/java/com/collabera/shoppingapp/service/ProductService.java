package com.collabera.shoppingapp.service;

import com.collabera.shoppingapp.model.Product;

public interface ProductService {

    void createProduct(Product newProduct);
    Iterable<Product> getProduct();
    void updateProduct(Long id, Product product);
    void deleteProduct(Long id);
}
